import React from 'react'
import Head from 'next/head'
import styles from '../styles/Home.module.css'

export default function About() {
    return (
        <>
            <Head>
                <title>Coder Wikipedia | About</title>
                <meta name='keywords' content='codes' />
            </Head>
            <div>
                <h1 className={styles.title}>About</h1>
            </div>
        </>
    )
}
