import React from 'react'

export default function Footer() {
    return (
        <div style={{textAlign:"center",paddingTop:"100px"}}>Copyright 2022 Coder Wikipedia</div>
    )
}
